import React, { useState, useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import './item-list.css';

const ItemList = ({ test }) => {
  const [property, setProperty] = useState(test)
  useEffect(() => {
    setProperty(test) //зберігаємо нові дані
  })
  return (
    <ul className="item-list list-group">
      {property === null ? <CircularProgress /> : property.map((item, index) => {
        return <li className="list-group-item" key={index * 3 + 'r'}> {item.name} </li>
      })}
    </ul>
  );

}

export default ItemList;
